export { };

declare module '@feng3d/objectview'
{
    interface OAVComponentParamMap
    {
        OAVComponentList: { component: 'OAVComponentList', componentParam: Object };
        OAVEntityName: { component: 'OAVEntityName', componentParam: Object };
    }
}
